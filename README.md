# Reminder Bot

## Описание

Приложение которое позволят сохранять напоминания, записанные в телеграмм бота.
Напоминания сохраняются в подключенную базу данных.
Взаимодействия с телеграммом основано на фреймворке для ботов - [Grammy](https://grammy.dev/).
Сервисы разбиты на классы и модули, и взаимодействуют между собой через Dependency injection.
Дальнейшие взаимодействие осуществляется через брокер сообщений - RabbitMQ

Сервис, который позволят обрабатывать список напоминаний и отсылать напоминалки: [В разработке...](https://gitlab.com/zongdm/reminder-schedule)

## Зависимости

-   Node 14
-   Typescript
-   Docker, Docker Compose
-   MongoDB
-   Git

## Установка

```bash
$ npm i

# OR use for development to make sure you're doing a clean install of your dependencies
$ npm ci
```

## Запуск приложения

Edit .env or docker-compose.yml environment

```bash
# development
$ npm run start:dev

# production mode
$ npm run build
$ npm run start:prod
```

## TODO

-   [ ] Дописать функционал сохранения постоянных напоминаний. Нужно доработать их логику, и напоминат не только раз в год, а например, каждый вторник, в начале месяца и т.д.
-   [ ] В RabbitMQ Service реализовать паттерн распределения по топикам, учитывая \* и # (пока такой необходимости нет, но это хорошая практика)
