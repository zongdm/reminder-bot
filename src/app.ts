import 'reflect-metadata'
import { Bot } from './bot'
import { Bot as GrammyBot, GrammyError, session } from 'grammy'
import { inject, injectable } from 'inversify'
import { IConfigService } from './config/config.interface'
import { IBotContext } from './context/context.interface'
import { IDatabaseService } from './database/database.interface'
import { ILoggerService } from './logger/logger.interface'
import { StartComposer } from './composer/start.composer'
import { TYPES } from './types'
import { conversations } from '@grammyjs/conversations'
import { ReminderComposer } from './composer/reminder.composer'
import { IRMQService } from './rabbitmq/rmq.interface'
import { RMQController } from './rabbitmq/rmq.controller'

@injectable()
export class App {
	bot: GrammyBot<IBotContext>

	constructor(
		@inject(TYPES.Bot) private botService: Bot,
		@inject(TYPES.ILoggerService) private logger: ILoggerService,
		@inject(TYPES.IConfigService) private configService: IConfigService,
		@inject(TYPES.IDatabaseService) private databaseService: IDatabaseService,
		@inject(TYPES.IRMQService) private rmqService: IRMQService,
		@inject(TYPES.RMQController) private rmqController: RMQController,
		@inject(TYPES.StartComposer) private startComposer: StartComposer,
		@inject(TYPES.ReminderComposer) private reminderComposer: ReminderComposer,
	) {
		this.bot = this.botService.bot
	}

	useSession() {
		this.bot.use(
			session({
				initial() {
					return {}
				},
			}),
		)
	}

	useComposer() {
		this.bot.use(this.startComposer.init())
		this.bot.use(this.reminderComposer.init())
	}

	useConversations() {
		this.bot.use(conversations())
	}

	useCatch() {
		this.bot.catch((err) => {
			const ctx = err.ctx
			this.logger.error(`Error while handling update ${ctx.update.update_id}:`)
			const e = err.error
			if (e instanceof GrammyError) {
				this.logger.error('Error in request:', e.description)
			} else if (e instanceof Error) {
				this.logger.error('Error:', e)
			} else {
				this.logger.error('Unknown Error:', e)
			}
		})
	}

	async init() {
		await this.databaseService.connect({
			hostname: this.configService.get('MONGODB_HOSTNAME'),
			port: Number(this.configService.get('MONGODB_PORT')),
			dbName: this.configService.get('MONGODB_DBNAME'),
			user: this.configService.get('MONGODB_USER'),
			pass: this.configService.get('MONGODB_PASSWORD'),
			authSource: 'admin',
		})
		await this.rmqService.connect({
			exchangeName: this.configService.get('RMQ_EXCHANGE'),
			queueName: this.configService.get('RMQ_QUEUE'),
			serviceName: this.configService.get('RMQ_SERVICENAME'),
			prefetchCount: Number(this.configService.get('RMQ_PREFETCHCOUNT')),
			connections: [
				{
					hostname: this.configService.get('RMQ_HOSTNAME'),
					username: this.configService.get('RMQ_USERNAME'),
					password: this.configService.get('RMQ_PASSWORD'),
				},
			],
		})
		await this.rmqController.init()

		this.useSession()
		this.useConversations()
		this.useComposer()
		this.useCatch()

		this.bot.start()
		this.logger.log('[Bot App] Приложение запущено')
	}
}
