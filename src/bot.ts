import { inject, injectable } from 'inversify'
import 'reflect-metadata'
import { Bot as GrammyBot } from 'grammy'
import { IBotContext } from './context/context.interface'
import { TYPES } from './types'
import { IConfigService } from './config/config.interface'

@injectable()
export class Bot {
	private readonly _bot: GrammyBot<IBotContext>

	get bot() {
		return this._bot
	}

	constructor(@inject(TYPES.IConfigService) private configService: IConfigService) {
		this._bot = new GrammyBot(this.configService.get('TG_BOT_TOKEN'))
	}
}
