import { injectable } from 'inversify'
import { Composer } from 'grammy'
import { IBotContext } from '../context/context.interface'
import { ConversationFn, createConversation } from '@grammyjs/conversations'
import { ILoggerService } from '../logger/logger.interface'

@injectable()
export abstract class BaseComposer {
	private readonly _composer: Composer<IBotContext>

	constructor(private name: string, private loggerService: ILoggerService) {
		this._composer = new Composer()
	}

	get composer() {
		return this._composer
	}

	abstract conversationHandler(): void

	abstract handler(): void

	init(): Composer<IBotContext> {
		this.loggerService.log(`[Bot Composer] Обработчик "${this.name}" инициализирован`)
		return this._composer
	}

	protected createConversation(func: ConversationFn<IBotContext>) {
		this._composer.use(createConversation(func.bind(this), { id: func.name }))
	}
}
