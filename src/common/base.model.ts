import { injectable } from 'inversify'
import { Model, Schema, model, SchemaDefinition, SchemaDefinitionType } from 'mongoose'

@injectable()
abstract class BaseSchema<T> {
	private readonly _schema: Schema<T>

	get schema() {
		return this._schema
	}

	constructor() {
		this._schema = new Schema<T>().set('versionKey', false)
	}
}

@injectable()
export abstract class BaseModel<T> extends BaseSchema<T> {
	private _model!: Model<T>

	get model() {
		return this._model
	}

	protected createModel(name: string, schemaObj: SchemaDefinition<SchemaDefinitionType<T>> | Schema<T>) {
		this.schema.add(schemaObj)
		this._model = model<T>(name, this.schema)
	}
}
