import { Conversation } from '@grammyjs/conversations'
import { ru } from 'chrono-node'
import { InlineKeyboard, Keyboard } from 'grammy'
import { inject, injectable } from 'inversify'
import { BaseComposer } from '../common/base.composer'
import { IBotContext } from '../context/context.interface'
import { ILoggerService } from '../logger/logger.interface'
import { INotificationService } from '../notification/notification.interface'
import { IReminderService } from '../reminder/reminder.service.interface'
import { TYPES } from '../types'
import { IUserService } from '../user/user.service.interface'

@injectable()
export class ReminderComposer extends BaseComposer {
	constructor(
		@inject(TYPES.ILoggerService) private logger: ILoggerService,
		@inject(TYPES.IUserService) private userService: IUserService,
		@inject(TYPES.IReminderService) private reminderService: IReminderService,
		@inject(TYPES.ITelegramNotificationService) private notificationService: INotificationService,
	) {
		super('Напоминания', logger)
	}

	conversationHandler(): void {
		this.createConversation(this.createTempReminder)
		this.createConversation(this.createPermanentReminder)
		this.createConversation(this.getReminders)
	}

	handler(): void {
		this.composer.on(':text').hears(/^временное$/gim, async (ctx) => {
			try {
				await ctx.conversation.enter(this.createTempReminder.name)
			} catch (e) {
				console.log(e)
			}
		})

		this.composer.on(':text').hears(/^постоянное$/gim, async (ctx) => {
			await ctx.conversation.enter(this.createPermanentReminder.name)
		})

		this.composer.on(':text').hears(/^список$/gim, async (ctx) => {
			await ctx.conversation.enter(this.getReminders.name)
		})

		this.composer.callbackQuery('btn-exit', async (ctx) => {
			await ctx.conversation.exit()
			await ctx.answerCallbackQuery()
			await ctx.deleteMessage()
		})
	}

	private async createTempReminder(conversation: Conversation<IBotContext>, ctx: IBotContext) {
		let time: Date | null
		const user = await this.userService.getUserByTgId(ctx.from.id)
		if (!user) {
			ctx.reply('Упс-с, кажется, вас еще нет в моем списке. Введите /start')
			return
		}

		// 1. Читаем время которое ввел пользователь
		await ctx.reply(`Введите время, когда нужно будет напомнить⏰`, {
			reply_markup: { remove_keyboard: true },
		})
		const {
			message: { text: timeText },
		} = await conversation.waitFor('message:text')
		time = ru.parseDate(timeText)

		// 2. Проверяем правильно ли мы распарсили текст, создаем клавиатуру для пользователя
		await conversation.external(() => this.logger.log(`Message: ${timeText}; Parse: ${time}; Date: ${new Date()}`)) //Вынужденная обертка, чтобы лог не повторялся

		while (!time) {
			await ctx.reply('Не удалось распознать время. Попробуйте написать еще раз, более подробнее', {
				reply_markup: new InlineKeyboard().text('выйти', 'btn-exit'),
			})
			const {
				message: { text: timeText },
			} = await conversation.waitFor('message:text')
			time = ru.parseDate(timeText)
			this.logger.log(`Try Again Message: ${timeText}; Parse: ${time}; Date: ${new Date()}`)
		}

		await ctx.reply(
			`Создать уведомление ${time.toLocaleString()}?\n\nВведите сообщение которое нужно будет напомнить💬`,
			{
				reply_markup: new InlineKeyboard().text('некорректное время', 'btn-exit'), //TODO Не должно быть выхода
			},
		)

		// 3. Читаем сообщение которое нужно будет напомнить
		const {
			message: { text: msgText },
		} = await conversation.waitFor('message:text')

		// 4. Создаем напоминаиние
		await this.reminderService.createReminder({
			userId: user._id,
			date: time,
			text: msgText,
			temp: true,
		})
		await ctx.reply(`Создано временное уведомление\nВремя: ${time.toLocaleString()}\nСообщение: ${msgText}`, {
			reply_markup: new Keyboard().text('Временное').text('Постоянное').resized(),
		})
		return
	}

	//TODO Make this method
	private async createPermanentReminder(conversation: Conversation<IBotContext>, ctx: IBotContext) {
		await ctx.reply(`В разработке...`)
		return
	}

	private async getReminders(conversation: Conversation<IBotContext>, ctx: IBotContext) {
		const user = await this.userService.getUserByTgId(ctx.from.id)
		if (!user) {
			ctx.reply('Упс-с, кажется, вас еще нет в моем списке. Введите /start')
			return
		}

		const reminders = await this.reminderService.getReminders({ userId: user._id })

		const result = []
		for (const reminder of reminders) {
			result.push(`Время: ${reminder.date.toLocaleString()}\nТекст: ${reminder.text}`)
		}

		ctx.reply(result.join('\n\n'))
		return
	}
}
