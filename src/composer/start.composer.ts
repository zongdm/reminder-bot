import { inject, injectable } from 'inversify'
import { ILoggerService } from '../logger/logger.interface'
import { TYPES } from '../types'
import { IUserService } from '../user/user.service.interface'
import { BaseComposer } from '../common/base.composer'
import { Conversation } from '@grammyjs/conversations'
import { IBotContext } from '../context/context.interface'
import { Keyboard } from 'grammy'

@injectable()
export class StartComposer extends BaseComposer {
	constructor(
		@inject(TYPES.ILoggerService) private logger: ILoggerService,
		@inject(TYPES.IUserService) private userService: IUserService,
	) {
		super('Команда /start', logger)
	}

	conversationHandler(): void {
		this.createConversation(this.greeting)
	}

	handler(): void {
		this.composer.command('start', async (ctx) => {
			await ctx.conversation.enter(this.greeting.name)
		})
	}

	private async greeting(conversation: Conversation<IBotContext>, ctx: IBotContext) {
		const oldUser = await this.userService.getUserByTgId(ctx.from.id)
		if (oldUser) {
			await ctx.reply(`Привет уже веду твои напоминания, ${ctx.from.first_name}!`, {
				reply_markup: new Keyboard().text('временное').text('постоянное').resized(),
			})
			return
		} else {
			await ctx.reply(
				'Забываешь когда день рождения знакомых, важные даты в своей жизни? Тогда запиши их сюда, а я буду тебе напоминать!👋',
				{ reply_markup: new Keyboard().text('Давай!').resized() },
			)
		}
		await conversation.wait()
		await this.userService.createUser({ tgId: ctx.from.id, tgUsername: ctx.from.username })
		this.logger.log(`New user create tgId: ${ctx.from.id}, tgUsername: ${ctx.from.username}`)
		await ctx.reply(`Привет, ${ctx.from.first_name}. Создал карточку с твоими напоминаниями!`, {
			reply_markup: new Keyboard().text('Временное').text('Постоянное').resized(),
		})
		return
	}
}
