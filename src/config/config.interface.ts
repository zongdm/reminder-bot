export interface IConfigService {
	get(key: string): string | null
	getOrThrow(key: string): string | null
}

export interface ErrnoException extends Error {
	errno?: number
	code?: string
	path?: string
	syscall?: string
	stack?: string
}
