import { inject, injectable } from 'inversify'
import { config, DotenvParseOutput } from 'dotenv'
import { ErrnoException, IConfigService } from './config.interface'
import { join } from 'path'
import { TYPES } from '../types'
import { ILoggerService } from '../logger/logger.interface'

@injectable()
export class ConfigService implements IConfigService {
	private config: DotenvParseOutput

	constructor(@inject(TYPES.ILoggerService) private logger: ILoggerService) {
		const { error, parsed } = config({ path: join(__dirname, '/../../.env') })
		const parsedError = error as ErrnoException

		if (parsedError) {
			if (parsedError.code === 'ENOENT') {
				this.logger.log('[ConfigService] Локальный .env файл не найден')
			} else {
				throw new Error('[ConfigService] Ошибка при чтении локально файла с переменными')
			}
		}

		if (!parsed && parsedError.code !== 'ENOENT') {
			this.logger.warn('[ConfigService] Файл .env пустой')
		}

		this.config = parsed ?? {}
	}

	get(key: string): string {
		return this.config[key] ?? process.env[key]
	}

	getOrThrow(key: string): string {
		const value = this.get(key)
		if (!value) {
			throw new Error(`[ConfigService] Ключ ${key} не найден в файле .env`)
		}
		return value
	}
}
