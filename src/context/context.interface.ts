import { ConversationFlavor } from '@grammyjs/conversations'
import { Context, SessionFlavor } from 'grammy'

interface SessionData {
	city?: string
	notificationTime?: Date
}

export type IBotContext = Context & SessionFlavor<SessionData> & ConversationFlavor
