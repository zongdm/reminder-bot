export interface IDatabaseService {
	connect(options: IConnectionOptions): Promise<void>
	disconnect(): Promise<void>
}

export interface IConnectionOptions {
	hostname: string
	port: number
	dbName?: string
	authSource?: string
	user?: string
	pass?: string
	connectTimeout?: number
}
