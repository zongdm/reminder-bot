import { inject, injectable } from 'inversify'
import { connect, disconnect, set } from 'mongoose'
import { ILoggerService } from '../logger/logger.interface'
import { TYPES } from '../types'
import { IConnectionOptions, IDatabaseService } from './database.interface'

@injectable()
export class DatabaseService implements IDatabaseService {
	constructor(@inject(TYPES.ILoggerService) private logger: ILoggerService) {}

	async connect(options?: IConnectionOptions): Promise<void> {
		try {
			set('strictQuery', false)
			await connect(`mongodb://${options.hostname}:${options.port}`, {
				dbName: options.dbName ?? '',
				authSource: options.authSource ?? '',
				user: options.user ?? '',
				pass: options.pass ?? '',
				connectTimeoutMS: options.connectTimeout ?? 30000,
				ignoreUndefined: true, // Нужно для find метода, если параметры не передали
			})
			this.logger.log('[MongoDB] База данных успешно подключена')
		} catch (e) {
			throw new Error(`[MongoDB] Ошибка подключения базы данных' ${e instanceof Error ? e.message : e}`)
		}
	}

	async disconnect(): Promise<void> {
		await disconnect()
		this.logger.warn('[MongoDB] База данных отключена')
	}
}
