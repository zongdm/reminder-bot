export interface ILoggerService {
	log(...arg: any[]): void
	warn(...arg: any[]): void
	error(...arg: any[]): void
}
