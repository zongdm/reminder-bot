import { injectable } from 'inversify'
import { Logger, createLogger, format, transports } from 'winston'
import { ILoggerService } from './logger.interface'

@injectable()
export class LoggerService implements ILoggerService {
	private logger: Logger

	constructor() {
		this.logger = createLogger({
			level: 'info',
			format: format.combine(
				format.timestamp({
					format: 'DD-MMM-YYYY HH:mm:ss',
				}),
				format.errors({ stack: true }),
				format.align(),
				format.splat(),
				format.json(),
			),
			transports: [
				new transports.Console({
					format: format.combine(
						format.colorize(),
						format.printf((info) => `${info.level} : ${info.timestamp} : ${info.message}`),
					),
				}),
				new transports.File({ filename: './logs/error.log', level: 'error', maxsize: 10000000, maxFiles: 2 }), // 10 MB, 2 files
				new transports.File({ filename: './logs/combine.log', maxsize: 10000000, maxFiles: 5 }), // 10 MB, 5 files
			],
		})
	}

	public log(...arg: any[]): void {
		this.logger.log({
			level: 'info',
			message: `${arg.join(' ')}`,
		})
	}

	public warn(...arg: any[]): void {
		this.logger.warn({
			level: 'warn',
			message: `${arg.join(' ')}`,
		})
	}

	public error(...arg: any[]): void {
		this.logger.error({
			level: 'error',
			message: `${arg.join(' ')}`,
		})
	}
}
