import { Container, ContainerModule, interfaces } from 'inversify'
import { TYPES } from './types'
import { App } from './app'
import { Bot } from './bot'
import { IConfigService } from './config/config.interface'
import { ConfigService } from './config/config.service'
import { ILoggerService } from './logger/logger.interface'
import { LoggerService } from './logger/logger.service'
import { StartComposer } from './composer/start.composer'
import { IDatabaseService } from './database/database.interface'
import { DatabaseService } from './database/database.service'
import { UserModel } from './user/user.model'
import { IUserService } from './user/user.service.interface'
import { UserService } from './user/user.service'
import { ReminderModel } from './reminder/reminder.model'
import { ReminderComposer } from './composer/reminder.composer'
import { IReminderService } from './reminder/reminder.service.interface'
import { ReminderService } from './reminder/reminder.service'
import { INotificationService } from './notification/notification.interface'
import { TelegramNotificationService } from './notification/telegram-notification.service'
import { IRMQService } from './rabbitmq/rmq.interface'
import { RMQService } from './rabbitmq/rmq.service'
import { RMQController } from './rabbitmq/rmq.controller'

export const appBindings = new ContainerModule((bind: interfaces.Bind) => {
	bind<App>(TYPES.App).to(App).inSingletonScope()
	bind<Bot>(TYPES.Bot).to(Bot).inSingletonScope()
	bind<ILoggerService>(TYPES.ILoggerService).to(LoggerService).inSingletonScope()
	bind<IConfigService>(TYPES.IConfigService).to(ConfigService).inSingletonScope()
	bind<IDatabaseService>(TYPES.IDatabaseService).to(DatabaseService).inSingletonScope()
	bind<IRMQService>(TYPES.IRMQService).to(RMQService).inSingletonScope()
	bind<RMQController>(TYPES.RMQController).to(RMQController).inSingletonScope()
	bind<UserModel>(TYPES.UserModel).to(UserModel).inSingletonScope()
	bind<IUserService>(TYPES.IUserService).to(UserService).inSingletonScope()
	bind<StartComposer>(TYPES.StartComposer).to(StartComposer).inSingletonScope()
	bind<ReminderModel>(TYPES.ReminderModel).to(ReminderModel).inSingletonScope()
	bind<ReminderComposer>(TYPES.ReminderComposer).to(ReminderComposer).inSingletonScope()
	bind<IReminderService>(TYPES.IReminderService).to(ReminderService).inSingletonScope()
	bind<INotificationService>(TYPES.ITelegramNotificationService).to(TelegramNotificationService).inSingletonScope()
})

async function bootstrap() {
	const appContainer = new Container()
	appContainer.load(appBindings)
	const app = appContainer.get<App>(TYPES.App)
	await app.init()
}
bootstrap()
