import { NotifyDto } from './dto/notify.dto'

export interface INotificationService {
	notify(dto: NotifyDto): Promise<void>
}
