import { inject, injectable } from 'inversify'
import { Bot } from '../bot'
import { TYPES } from '../types'
import { NotifyDto } from './dto/notify.dto'
import { INotificationService } from './notification.interface'

@injectable()
export class TelegramNotificationService implements INotificationService {
	constructor(@inject(TYPES.Bot) private botService: Bot) {}

	async notify(dto: NotifyDto): Promise<void> {
		await this.botService.bot.api.sendMessage(dto.id, dto.msg)
	}
}
