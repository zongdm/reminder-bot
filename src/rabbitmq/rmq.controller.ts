import { inject, injectable } from 'inversify'
import { INotificationService } from '../notification/notification.interface'
import { IReminderService } from '../reminder/reminder.service.interface'
import { TYPES } from '../types'
import { IRMQService } from './rmq.interface'

@injectable()
export class RMQController {
	constructor(
		@inject(TYPES.IRMQService) private rmqService: IRMQService,
		@inject(TYPES.IReminderService) private reminderService: IReminderService,
		@inject(TYPES.ITelegramNotificationService) private notificationService: INotificationService,
	) {}

	public async init() {
		await this.rmqService.bindRoutingKey(
			'reminders.query',
			this.reminderService.getReminders.bind(this.reminderService),
		)
		await this.rmqService.bindRoutingKey(
			'delete-reminder.command',
			this.reminderService.deleteReminder.bind(this.reminderService),
		)
		await this.rmqService.bindRoutingKey(
			'telegram-message.event',
			this.notificationService.notify.bind(this.notificationService),
		)
	}
}
