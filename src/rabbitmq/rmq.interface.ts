/* eslint-disable @typescript-eslint/ban-types */
import { Channel, Options } from 'amqplib'

export interface IRMQService {
	connect: (options?: IRMQOptions) => Promise<void>
	send: <IMessage, IReply>(topic: string, message: IMessage) => Promise<IReply>
	notify: <IMessage>(topic: string, message: IMessage) => Promise<void>
	ack: (...params: Parameters<Channel['ack']>) => ReturnType<Channel['ack']>
	nack: (...params: Parameters<Channel['nack']>) => ReturnType<Channel['nack']>
	bindRoutingKey: (topic: string, func: Function) => Promise<void>
	disconnect: () => Promise<void>
}

export interface IRMQOptions {
	exchangeName?: string
	queueName?: string
	queueArguments?: {
		[key: string]: string
	}
	connections?: [IRMQConnection]
	prefetchCount?: number
	isGlobalPrefetchCount?: boolean
	queueOptions?: Options.AssertQueue
	isQueueDurable?: boolean
	isExchangeDurable?: boolean
	assertExchangeType?: Parameters<Channel['assertExchange']>[1]
	exchangeOptions?: Options.AssertExchange
	reconnectTimeInSeconds?: number
	heartbeatIntervalInSeconds?: number
	messagesTimeout?: number
	serviceName?: string
}

export interface IRMQConnection {
	username: string
	password: string
	hostname: string
	protocol?: RMQ_PROTOCOL
	port?: number
	vhost?: string
}

export enum RMQ_PROTOCOL {
	AMQP = 'amqp',
	AMQPS = 'amqps',
}

export interface IRMQRouter {
	route: string
	propertyKey: any
}
