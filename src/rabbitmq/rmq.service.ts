/* eslint-disable @typescript-eslint/ban-types */
import { Channel, Message } from 'amqplib'
import * as amqp from 'amqp-connection-manager'
import { AmqpConnectionManager, ChannelWrapper } from 'amqp-connection-manager'
import { randomUUID } from 'node:crypto'
import EventEmitter from 'node:events'
import { IRMQOptions, IRMQService } from './rmq.interface'
import { inject, injectable } from 'inversify'
import { TYPES } from '../types'
import { ILoggerService } from '../logger/logger.interface'

//TODO implement method getRouteByTopic, now is direct
@injectable()
export class RMQService implements IRMQService {
	private server: AmqpConnectionManager
	private clientChannel: ChannelWrapper
	private subscriptionChannel: ChannelWrapper
	private replyQueue = 'amq.rabbitmq.reply-to'
	private sendResponseEmitter: EventEmitter = new EventEmitter()
	private options: IRMQOptions
	private routes: Map<string, Function> = new Map<string, Function>()

	constructor(@inject(TYPES.ILoggerService) private logger: ILoggerService) {}

	public async connect(options?: IRMQOptions): Promise<void> {
		this.options = options

		return new Promise<void>(async (resolve) => {
			this.server = amqp.connect(...this.options.connections)

			this.server.on('connect', () => {
				this.logger.log('[RMQ] RabbitMQ успешно подключена')
			})

			this.server.on('disconnect', () => {
				this.logger.error('[RMQ] RabbitMQ отключена')
				this.sendResponseEmitter.removeAllListeners()
			})

			this.server.on('connectFailed', () => {
				throw new Error('[RMQ] Ошибка при подкючении к RabbitMQ')
			})

			await Promise.all([this.createClientChannel(), this.createSubscriptionChannel()])
			resolve()
		})
	}

	private async createClientChannel(): Promise<void> {
		return new Promise<void>((resolve) => {
			this.clientChannel = this.server.createChannel({
				json: false,
				setup: async (channel: Channel) => {
					await channel.consume(
						this.replyQueue,
						(msg: Message) => {
							this.sendResponseEmitter.emit(msg.properties.correlationId, msg)
						},
						{
							noAck: true,
						},
					)
					resolve()
				},
			})
		})
	}

	private async createSubscriptionChannel(): Promise<void> {
		return new Promise<void>((resolve) => {
			this.subscriptionChannel = this.server.createChannel({
				json: false,
				setup: async (channel: Channel) => {
					await channel.assertExchange(
						this.options.exchangeName,
						this.options.assertExchangeType ? this.options.assertExchangeType : 'topic',
						{
							durable: this.options.isExchangeDurable ?? true,
							...this.options.exchangeOptions,
						},
					)
					await channel.prefetch(this.options.prefetchCount ?? 0, this.options.isGlobalPrefetchCount ?? false)
					if (typeof this.options.queueName === 'string') {
						this.listen(channel)
					}
					resolve()
				},
			})
		})
	}

	public async send<IMessage, IReply>(topic: string, message: IMessage): Promise<IReply> {
		return new Promise(async (resolve, reject) => {
			try {
				const correlationId = randomUUID()
				const timeout = this.options.messagesTimeout ?? 30000
				const timerId = setTimeout(() => {
					reject(`[RMQ ERROR] Error timeout while sending to ${topic}`)
				}, timeout)

				this.sendResponseEmitter.once(correlationId, (msg: Message) => {
					clearTimeout(timerId)
					if (msg.properties?.headers?.['-x-error']) {
						reject('[RMQ ERROR] Error in headers')
					}
					const { content } = msg
					if (content.toString()) {
						resolve(JSON.parse(content.toString()))
					} else {
						reject('[RMQ ERROR] Content is empty')
					}
				})

				await this.clientChannel.publish(
					this.options.exchangeName,
					topic,
					Buffer.from(JSON.stringify(message)),
					{
						replyTo: this.replyQueue,
						appId: this.options.serviceName,
						correlationId,
					},
				)
			} catch (e) {
				reject(e)
			}
		})
	}

	public async notify<IMessage>(topic: string, message: IMessage): Promise<void> {
		await this.clientChannel.publish(this.options.exchangeName, topic, Buffer.from(JSON.stringify(message)), {
			appId: this.options.serviceName,
		})
	}

	private async listen(channel: Channel) {
		const queue = await channel.assertQueue(this.options.queueName, {
			durable: this.options.isQueueDurable ?? true,
			arguments: this.options.queueArguments ?? {},
			...this.options.queueOptions,
		})
		this.options.queueName = queue.queue

		await channel.consume(
			this.options.queueName,
			async (msg: Message) => {
				const route = this.routes.get(msg.fields.routingKey)
				if (route) {
					try {
						const data = JSON.parse(msg.content.toString())
						const result = await route(data)
						if (msg.properties.replyTo) await this.reply(msg, result)
						this.ack(msg)
					} catch (err) {
						err = err instanceof Error ? err.message : err
						if (msg.properties.replyTo) {
							this.subscriptionChannel.sendToQueue(msg.properties.replyTo, Buffer.from(''), {
								correlationId: msg.properties.correlationId,
								headers: {
									'-x-error': err,
								},
							})
							this.logger.error(`[${msg.fields.routingKey}] ${JSON.stringify(err)}`)
						}
					}
				} else {
					this.logger.error('[RMQ Error] No one route bind on this topic')
					this.ack(msg)
				}
			},
			{ noAck: false },
		)
	}

	public ack(...params: Parameters<Channel['ack']>): ReturnType<Channel['ack']> {
		return this.subscriptionChannel.ack(...params)
	}

	public nack(...params: Parameters<Channel['nack']>): ReturnType<Channel['nack']> {
		return this.subscriptionChannel.nack(...params)
	}

	private async reply(msg: Message, result: Buffer) {
		await this.subscriptionChannel.sendToQueue(msg.properties.replyTo, Buffer.from(JSON.stringify(result)), {
			appId: this.options.serviceName,
			correlationId: msg.properties.correlationId,
		})
	}

	public async disconnect() {
		this.sendResponseEmitter.removeAllListeners()
		await this.clientChannel.close()
		await this.subscriptionChannel.close()
		await this.server.close()
	}

	public async bindRoutingKey(topic: string, func: Function): Promise<void> {
		await this.subscriptionChannel.bindQueue(this.options.queueName, this.options.exchangeName, topic)
		this.routes.set(topic, func)
	}
}
