import { ObjectId } from 'mongoose'

export class ReminderCreateDto {
	userId: ObjectId
	date: Date
	text: string
	temp: boolean
}
