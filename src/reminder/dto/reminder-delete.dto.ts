import { ObjectId } from 'mongoose'

export class ReminderDeleteDto {
	id: ObjectId
}
