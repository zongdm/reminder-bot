import { ObjectId } from 'mongoose'

export class ReminderFindDto {
	userId?: ObjectId
	date?: {
		from: Date
		to?: Date
	}
	temp?: boolean
}
