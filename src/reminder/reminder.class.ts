import { ObjectId } from 'mongoose'
import { User } from '../user/user.class'

export class Reminder {
	_id: ObjectId
	userId: User
	date: Date
	text: string
	temp: boolean
}
