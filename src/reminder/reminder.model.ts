import { injectable } from 'inversify'
import { Schema } from 'mongoose'
import { BaseModel } from '../common/base.model'
import { Reminder } from './reminder.class'

@injectable()
export class ReminderModel extends BaseModel<Reminder> {
	constructor() {
		super()
		this.createModel('Reminder', {
			userId: { type: Schema.Types.ObjectId, ref: 'User' },
			date: { type: Date, required: true },
			text: { type: String, required: true },
			temp: { type: Boolean, required: true },
		})
	}
}
