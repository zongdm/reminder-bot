import { ReminderCreateDto } from './dto/reminder-create.dto'
import { ReminderDeleteDto } from './dto/reminder-delete.dto'
import { ReminderFindDto } from './dto/reminder-find.dto'
import { Reminder } from './reminder.class'

export interface IReminderService {
	createReminder(dto: ReminderCreateDto): Promise<Reminder>
	getReminders(dto?: ReminderFindDto): Promise<Reminder[] | null>
	deleteReminder(dto: ReminderDeleteDto): Promise<Reminder>
}
