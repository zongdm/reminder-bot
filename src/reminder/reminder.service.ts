import { inject, injectable } from 'inversify'
import { TYPES } from '../types'
import { ReminderCreateDto } from './dto/reminder-create.dto'
import { ReminderDeleteDto } from './dto/reminder-delete.dto'
import { ReminderFindDto } from './dto/reminder-find.dto'
import { Reminder } from './reminder.class'
import { ReminderModel } from './reminder.model'
import { IReminderService } from './reminder.service.interface'

@injectable()
export class ReminderService implements IReminderService {
	constructor(@inject(TYPES.ReminderModel) private reminderModel: ReminderModel) {}

	async createReminder(dto: ReminderCreateDto): Promise<Reminder> {
		return this.reminderModel.model.create({
			userId: dto.userId,
			date: dto.date,
			text: dto.text,
			temp: dto.temp,
		})
	}

	async getReminders(dto?: ReminderFindDto): Promise<Reminder[] | null> {
		const dateQuery = dto?.date?.to ? { $gte: dto.date.from, $lte: dto.date.to } : dto?.date?.from

		return this.reminderModel.model
			.find()
			.populate('userId')
			.where({
				userId: dto?.userId ?? undefined,
				date: dateQuery ?? undefined,
				temp: dto?.temp ?? undefined,
			})
	}

	async deleteReminder(dto: ReminderDeleteDto): Promise<Reminder> {
		return this.reminderModel.model.findByIdAndDelete(dto.id)
	}
}
