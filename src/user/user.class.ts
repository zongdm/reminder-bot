import { ObjectId } from 'mongoose'

export class User {
	_id: ObjectId
	tgId: number
	tgUsername: string
}
