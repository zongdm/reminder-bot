import { injectable } from 'inversify'
import { BaseModel } from '../common/base.model'
import { User } from './user.class'

@injectable()
export class UserModel extends BaseModel<User> {
	constructor() {
		super()
		this.createModel('User', {
			tgId: { type: Number, require: true, unique: true },
			tgUsername: { type: String },
		})
	}
}
