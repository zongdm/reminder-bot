import { UserCreateDto } from './dto/user-create.dto'
import { User } from './user.class'

export interface IUserService {
	createUser(dto: UserCreateDto): Promise<User>
	getUser(): Promise<User[]>
	getUserByTgId(tgId: number): Promise<User | null>
}
