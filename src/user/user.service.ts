import { inject, injectable } from 'inversify'
import { TYPES } from '../types'
import { UserCreateDto } from './dto/user-create.dto'
import { User } from './user.class'
import { UserModel } from './user.model'
import { IUserService } from './user.service.interface'

@injectable()
export class UserService implements IUserService {
	constructor(@inject(TYPES.UserModel) private userModel: UserModel) {}

	async createUser(dto: UserCreateDto): Promise<User> {
		return await this.userModel.model.create({
			tgId: dto.tgId,
			tgUsername: dto.tgUsername,
		})
	}

	async getUser(): Promise<User[]> {
		return this.userModel.model.find()
	}

	async getUserByTgId(tgId: number): Promise<User | null> {
		return this.userModel.model.findOne({ tgId })
	}
}
